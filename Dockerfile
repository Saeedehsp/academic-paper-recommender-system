FROM python:3.7
RUN apt update
RUN apt install -y ruby ruby-dev
RUN gem install specific_install
RUN gem specific_install -l https://github.com/mauromsl/anystyle-cli.git
WORKDIR /app
COPY ./src /app
RUN pip3 install -r /app/requirements.txt

