import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import {
  ToastPlugin,
  SpinnerPlugin,
  ProgressPlugin,
  LayoutPlugin,
  FormCheckboxPlugin
} from "bootstrap-vue";

Vue.use(ToastPlugin);
Vue.use(SpinnerPlugin);
Vue.use(ProgressPlugin);
Vue.use(LayoutPlugin)
Vue.use(FormCheckboxPlugin)

new Vue({
  render: h => h(App),
}).$mount('#app')