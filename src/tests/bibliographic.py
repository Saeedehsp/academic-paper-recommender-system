# from parsers.bibliographic import parse_bibliographic

import json
import logging
import re

from bibtexparser.bparser import BibTexParser
from bs4 import BeautifulSoup
from crossref.restful import Works
import requests

from doab import const
from doab.parsing_common import SubprocessMixin, CleanReferenceMixin

logger = logging.getLogger(__name__)


def parse_bibtex(reference, bibtex_parser=None):
    if bibtex_parser is None:
        bibtex_parser = BibTexParser()
    try:
        result = bibtex_parser.parse(reference).get_entry_list()[-1]
    except IndexError:
        #unable to parse
        result = None

    return result


class BaseReferenceParser(CleanReferenceMixin):
    """ A base class for implementing reference parsers
    ChildrenMustImplement
        parse_reference()
    :param book_path: (str) The path to the book to be parsed
    """
    # a variable that children can override to specify how good they are
    # compared to other parsers
    accuracy = 0

    @classmethod
    def parse_reference(cls, reference):
        return None


# class CermineParser(BaseReferenceParser, SubprocessMixin):
#     accuracy = 50
#     NAME = const.CERMINE
#     CMD = "cermine"
#     ARGS = [
#         "pl.edu.icm.cermine.bibref.CRFBibReferenceParser",
#         "-format", 'bibtex', "-reference",
#     ]
#
#     @classmethod
#     def parse_reference(cls, reference, bibtex_parser=None):
#         bibtex_reference = cls.call_cmd(reference)
#         logger.debug(f"Bibtex {bibtex_reference}")
#         result = parse_bibtex(bibtex_reference, bibtex_parser)
#         fail_message = f'{cls.NAME} was unable to pull a title from {reference}'
#
#         # append a full stop if there is no title returned and re-run
#         if not result or not 'title' in result or not result["title"]:
#             # Cermine struggles with references missing trailling fullstop
#             if not reference.endswith("."):
#                 retry = reference + '.'
#                 return cls.parse_reference(retry)
#             else:
#                 logger.debug(fail_message)
#                 result = None
#
#
#         return result


class AnystyleParser(BaseReferenceParser, SubprocessMixin):
    accuracy = 60 # Works better than Cermine for non-english references
    NAME = const.ANYSTYLE
    CMD = "anystyle"
    ARGS = ["-f", "bib", "parse_reference"]

    @classmethod
    def parse_reference(cls, reference, bibtex_parser=None):
        if reference.startswith("-"):
            reference = f'"{reference}"'
        bibtex_reference = cls.call_cmd(reference)
        logger.debug(f"Bibtex {bibtex_reference}")
        result = parse_bibtex(bibtex_reference)
        fail_message = f'{cls.NAME} was unable to pull a title from {reference}'

        if not result or not 'title' in result or not result["title"]:
            logger.debug(fail_message)
            return None

        return result


# class HTTPBasedParserMixin(BaseReferenceParser):
#     """ Mixin for requesting references to be parsed by an HTTP service"""
#     REQ_METHOD = ""
#     SVC_URL = ""
#
#     def call_svc(self, params=None, data=None):
#         svc_caller = getattr(requests, self.REQ_METHOD)
#         response = svc_caller()
#         return response
#
#
# class CrossrefParser(HTTPBasedParserMixin):
#     """A parser that matches DOIS and retrieves metadata via Crossref API"""
#     accuracy = 100
#     NAME = const.CROSSREF
#
#     @classmethod
#     def parse_reference(cls, reference, bibtex_parser=None):
#         ret = None
#         crossref_match = const.DOI_RE.search(reference)
#
#         if crossref_match:
#             works = Works(etiquette=const.CROSSREF_ETIQUETTE)
#             doi = works.doi(crossref_match.group(0))
#
#             if doi:
#                 ret = {'raw_reference': reference}
#                 ret['doi'] = crossref_match.group(0)
#
#                 ret['author'] = ''
#                 if 'author' in doi:
#                     ret['author'] = ', '.join(
#                         [
#                             f'{author.get("given", "")} {author.get("family", "")}'
#                             for author in doi['author']
#                         ]
#                     )
#
#                 if 'title' in doi and doi["title"]:
#                     ret['title'] = doi['title'][0]
#                 else:
#                     logger.warning(f"No Title available for {crossref_match} ")
#                     return None
#
#
#                 if 'container-title' in doi and doi['container-title']:
#                     ret['journal'] = doi['container-title'][0]
#
#                 if 'volume' in doi:
#                     ret['volume'] = doi['volume'][0]
#
#                 if 'published-online' in doi:
#                     ret['year'] = doi['published-online']['date-parts'][0][0]
#
#         return ret
#
#
# class BloomsburyAcademicParser(BaseReferenceParser):
#     accuracy = 75
#     NAME = const.BLOOMSBURY_ACADEMIC
#
#
#     def parse_reference(cls, reference):
#         # create a soup version of the reference
#         souped = BeautifulSoup(reference, 'html.parser')
#         formatted_reference = {}
#
#         # authors (and editors)
#         authors = []
#         for soup_author in souped.find_all('span', {'class': 'author'}):
#             try:
#                 authors.append(f'{soup_author.find("span", {"class":"firstname"}).get_text()} '
#                                f'{soup_author.find("span", {"class":"surname"}).get_text()}')
#             except:
#                 pass
#         for soup_author in souped.find_all('span', {'class': 'editor'}):
#             try:
#                 authors.append(f'{soup_author.find("span", {"class":"firstname"}).get_text()} '
#                                f'{soup_author.find("span", {"class":"surname"}).get_text()}')
#             except:
#                 pass
#         formatted_reference['author'] = ' ,'.join(authors)
#
#         # year
#         try:
#             formatted_reference['year'] = souped.find('span', {'class': 'pubdate'}).get_text()
#         except:
#             formatted_reference['year'] = ''
#
#         # we initially populate title and journal with the same field
#         try:
#             formatted_reference['title'] = souped.find('span', {'class': 'italic'}).get_text()
#             formatted_reference['journal'] = souped.find('span', {'class': 'italic'}).get_text()
#         except:
#             formatted_reference['title'] = ''
#             formatted_reference['journal'] = ''
#
#         # volume
#         try:
#             formatted_reference['volume'] = souped.find('span', {'class': 'volumenum'}).get_text()
#         except:
#             formatted_reference['volume'] = ''
#
#         # determine the type of entry
#         # if it contains ", in", it's a book chapter
#         book_regex = re.compile(r'‘(<i>)*(.+?)(<\/i>)*(<\/span>)*’, in', re.MULTILINE | re.DOTALL)
#         match = book_regex.search(reference)
#         is_book = False
#         if match:
#             is_book = True
#             formatted_reference['title'] = match.group(2)
#
#         if not is_book:
#             # journal articles
#             journal_regex = re.compile(r'atitle=(.+?)&', re.MULTILINE | re.DOTALL)
#             match = journal_regex.search(reference)
#             if match and not '&amp;aulast' in match.group(1):
#                 formatted_reference['title'] = match.group(1)
#             else:
#                 journal_regex = re.compile(r'‘(.+?)’', re.MULTILINE | re.DOTALL)
#                 match = journal_regex.search(reference)
#                 if match:
#                     formatted_reference['title'] = match.group(1)
#         return formatted_reference
#
#
# class CambridgeCoreParser(BaseReferenceParser):
#     accuracy = 85
#     NAME = const.CAMBRIDGE_CORE
#
#     def parse_reference(cls, reference):
#         try:
#             reference_json = json.loads(reference)
#         except json.decoder.JSONDecodeError:
#             logger.warning(f"Not valid JSON reference: {reference}")
#             return None
#
#         formatted_reference = {}
#
#         # year
#         formatted_reference['year'] = reference_json['atom:content']['m:pub-year']
#
#         # title
#         if reference_json['atom:content']['m:title'] is not None \
#                 and reference_json['atom:content']['m:title'] != '':
#             formatted_reference['title'] = reference_json['atom:content']['m:title']
#         elif reference_json['atom:content']['m:book-title'] is not None \
#                 and reference_json['atom:content']['m:book-title'] != '':
#             formatted_reference['title'] = reference_json['atom:content']['m:book-title']
#
#         # log the raw reference
#         formatted_reference['raw_reference'] = reference_json['atom:content']['m:display']
#
#         # journal
#         if reference_json['atom:content']['m:journal-title'] != '' \
#                 and reference_json['atom:content']['m:journal-title'] is not None:
#             formatted_reference['journal'] = reference_json['atom:content']['m:journal-title']
#
#         # authors
#         formatted_reference['author'] = ''
#         for author in reference_json['atom:content']['m:authors']:
#             formatted_reference['author'] += f'{author["content"]}, '
#
#         # DOI where it exists
#         if reference_json['atom:content']['m:dois'] is not None \
#                 and len(reference_json['atom:content']['m:dois']) > 0 \
#                 and reference_json['atom:content']['m:dois'][0]['content'] != '':
#             formatted_reference['doi'] = reference_json['atom:content']['m:dois'][0]['content']
#
#         # volume
#         if reference_json['atom:content']['m:journal-volume'] != '' and \
#                 reference_json['atom:content']['m:journal-volume'] is not None:
#             formatted_reference['volume'] = reference_json['atom:content']['m:journal-volume']
#
#         formatted_reference['parser'] = cls.NAME
#
#         return formatted_reference


# x=parse_bibliographic([
#     """[1]      A.B. Chatfield, ”Fundamentals of High Accuracy Inertial Navigation", American Institute of Aeronautics and Astronautics, 1997.""",
#     """[2]      S. Kim, K. Choi, S. Lee, J. Choi, T. Hwang, B. Jang, and J. Lee, "A Bimodal Approach for Land Vehicle Localization", ETRI Journal, vol. 26, no. 5, Oct. 2004, pp. 497-500.""",
#     """[3]      S.Y. Cho, B.D. Kim, Y.S. Cho, and W.S. Choi, "Multi-model Switching for Car Navigation Containing Low-Grade IMU and GPS Receiver", ETRI Journal, vol. 29, no. 5, Oct. 2007, pp. 688-690.""",
#     """[4]      M.S. Grewal, L.R. Weill and A.P. Andrews, "Global Positioning Systems, Inertial Navigation, and Integration", John Wiley & Sons, Inc ., 2001.""",
#     """[5]      W.R. Baker and R.W. Clem, "Terrain contour matching (TERCOM) premier", ASP-TR-77-61, Aeronautical Systems Division, Wright-Patterson, 1997."""
# ])
#

# def parse_bibliographic(bibliographic):
#     any_style = AnystyleParser()
#     return any_style.parse_bibliographic(bibliographic)
#
# print(parse_bibliographic('A.B. Chatfield, ”Fundamentals of High Accuracy Inertial Navigation", American Institute of Aeronautics and Astronautics, 1997.'))



# bib = ['A. Semlyen, "Contributions to the theory\nof calculation of electromagnetic transients on transmission lines with frequency\ndependent parameters", <em>IEEE Trans. Power App. Syst.</em>, vol. PAS-100, no. 2, pp. 848-856, Feb. 1981.', 'J. R. Marti, "Accurate modelling of frequency-dependent\ntransmission lines in electromagnetic transient simulations", <em>IEEE Trans. Power App. Syst.</em>, vol. PAS-101, no. 1, pp. 147-157, Jan. 1982.', 'B. Gustavsen, J. Sletbak and T. Henriksen, "Calculation of electromagnetic\ntransients in transmission cables and lines taking frequency dependent effects\naccurately into account", <em>IEEE Trans. Power Del.</em>, vol. 10, no. 2, pp. 1076-1084, Apr. 1995.', 'T. Noda, N. Nagaoka and A. Ametani, "Phase domain modeling of frequency-dependent\ntransmission lines by means of an ARMA model", <em>IEEE Trans. Power Del.</em>, vol. 11, no. 1, pp. 401-411, Jan. 1996.', 'F. Rachidi, C. A. Nucci, M. Ianoz and C. Mazzetti, "Influence of a lossy ground on lightning-induced\nvoltages on overhead lines", <em>IEEE Trans. Electromagn. Compat.</em>, vol. 38, no. 3, pp. 250-264, Aug. 1996.', 'A. Morched, B. Gustavsen and M. Tartibi, "A universal model for accurate\ncalculation of electromagnetic transients on overhead lines and underground\ncables", <em>IEEE Trans. Power Del.</em>, vol. 14, no. 3, pp. 1032-1038, Jul. 1999.', 'F. Rachidi, C. A. Nucci and M. Ianoz, "Transient analysis of multiconductor lines\nabove a lossy ground", <em>IEEE Trans. Power Del.</em>, vol. 14, no. 1, pp. 294-302, Jan. 1999.', 'M. Paolone, C. A. Nucci, E. Petrache and F. Rachidi, "Mitigation of lightning-induced\novervoltages in medium voltage distribution lines by means of periodical grounding\nof shielding wires and of surge arresters: Modelling and experimental validation", <em>IEEE Trans. Power Del.</em>, vol. 19, no. 1, pp. 423-431, Jan. 2004.', 'B. Gustavsen, "Computer code for rational\napproximation of frequency dependent admittance matrices", <em>IEEE Trans. Power Del.</em>, vol. 17, no. 4, pp. 1093-1098, Oct. 2002.', 'T. Noda, "Identification of a multiphase\nnetwork equivalent for electromagnetic transient calculations using partitioned\nfrequency response", <em>IEEE Trans. Power Del.</em>, vol. 20, no. 2, pp. 1134-1142, Apr. 2005.', 'A. Morched, L. Mart and J. Ottevangers, "A high-frequency transformer\nmodel for the EMTP", <em>IEEE Trans. Power Del.</em>, vol. 8, no. 3, pp. 1615-1626, Jul. 1993.', 'P. T. M. Vaessen, "Transformer model for high\nfrequencies", <em>IEEE Trans. Power Del.</em>, vol. 3, no. 4, pp. 1761-1768, Oct. 1988.', 'A. Borghetti, A. S. Morched, F. Napolitano, C. A. Nucci and M. Paolone, "Lightning-induced overvoltages\ntransferred through distribution power transformers", <em>IEEE Trans. Power Del.</em>, vol. 24, no. 1, pp. 360-372, Jan. 2009.', 'L. Grcev and M. Heimbach, "Frequency dependent and transient\ncharacteristics of substation grounding systems", <em>IEEE Trans. Power Del.</em>, vol. 12, no. 1, pp. 172-178, Jan. 1997.', 'L. Grcev, "Computer analysis of transient\nvoltages in large grounding systems", <em>IEEE Trans. Power Del.</em>, vol. 11, no. 2, pp. 815-823, Apr. 1996.', 'K. Sheshyekani, S. H. H. Sadeghi, R. Moini, F. Rachidi and M. Paolone, "Analysis of transmission lines\nwith arrester termination considering the frequency-dependence of grounding\nsystems", <em>IEEE Trans. Electromagn. Compat.</em>, vol. 51, no. 4, pp. 986-994, Nov. 2009.', 'A. Semlyen and A. Dabuleanu, "Fast and accurate switching\ntransient calculations on transmission lines with ground return using recursive\nconvolutions", <em>IEEE Trans. Power App. Syst.</em>, vol. PAS-94, no. 2, pp. 561-571, Mar./Apr. 1975.', 'R. J. Luebbers, F. P. Hunsberger, K. S. Kunz, R. B. Standler and M. Schneider, "A frequency-dependent finite-difference\ntime-domain formulation for dispersive materials", <em>IEEE Trans. Electromagn. Compat.</em>, vol. 32, no. 3, pp. 222-227, Aug. 1990.', 'B. Gustavsen and A. Semlyen, "Rational approximation of frequency\ndomain responses by vector fitting", <em>IEEE Trans. Power Del.</em>, vol. 14, no. 3, pp. 1052-1061, Jul. 1999.', 'A. Semlyen and B. Gustavsen, "Vector fitting by pole relocation\nfor the state equation approximation of non-rational transfer matrices", <em>Circuits Syst. Signal Process.</em>, vol. 19, no. 6, pp. 549-566, Nov. 2000.', 'B. Gustavsen, "Wide band modeling of power\ntransformers", <em>IEEE Trans. Power Del.</em>, vol. 19, no. 1, pp. 414-422, Jan. 2004.', 'B. Gustavsen, "Improving the pole relocating\nproperties of vector fitting", <em>IEEE Trans. Power Del.</em>, vol. 21, no. 3, pp. 1587-1592, Jul. 2006.', 'B. Gustavsen and A. Semlyen, "Enforcing passivity for admittance\nmatrices approximated by rational functions", <em>IEEE Trans. Power Syst.</em>, vol. 16, no. 1, pp. 97-104, Feb. 2001.', 'L. De Tommasi, M. de Magistris, D. Deschrijver and T. Dhaene, "An algorithm for direct identification\nof passive transfer matrices with positive real fractions via convex programming", <em>Int. J. Numer. Modelling: Electron. Networks Devices and Fields</em>, vol. 24, no. 4, 2011.', 'M. L. Van Blaricum and R. Mittra, "Problem and solutions associated\nwith Prony\'s method for processing transient data", <em>IEEE Trans. Antennas Propag.</em>, vol. AP-26, no. 1, pp. 174-182, Jan. 1978.', 'V. K. Jain, T. K. Sarkar and D. D. Weiner, "Rational modeling by pencil-of-function\nmethod", <em>IEEE Trans. Acoust. Speech Signal Process.</em>, vol. ASSP-31, no. 3, pp. 564-573, Jun. 1983.', 'Y. Hua and T. K. Sarkar, "Generalized pencil-of-function\nmethod for extracting poles of an EM system from its transient response", <em>IEEE Trans. Antennas Propag.</em>, vol. 37, no. 2, pp. 229-234, Feb. 1978.', 'Y. Hua and T. K. Sarkar, "Matrix pencil method for estimating\nparameters of exponentially damped/undamped sinusoids in noise", <em>IEEE Trans. Acoust. Speech Signal Process.</em>, vol. 38, no. 5, pp. 814-824, May 1990.', 'T. K. Sarkar and O. Pereira, "Using the matrix pencil method\nto estimate the parameters of a sum of complex exponentials", <em>IEEE Antennas Propag. Mag.</em>, vol. 37, no. 1, pp. 48-55, Feb. 1995.', 'H. Shi, "A closed-form approach to the\ninverse Fourier transform and its applications", <em>IEEE Trans. Electromag. Compat.</em>, vol. 50, no. 3, pp. 669-677, Aug. 2008.', 'K. Sheshyekani, H. R. Karami, P. Dehkhoda, F. Rachidi, R. Kazemi, S. H. H. Sadeghi, et al., "A new method for the inclusion of frequency\ndomain responses in time domain codes", <em>Int. Conf. Power Syst. Transients</em>, 2011-Jun.-1417.', 'A. V. Oppenheim, A. W. Willsky and I. T. Young, Signals and Systems, NJ, Upper Saddle River:Prentice-Hall, 1983.', "<em>User's Guide for the Vector Fitting</em>,  [online]  Available: http://www.energy.sintef.no/Produkt/VECTFIT.", 'B. Gustavsen and C. Heitz, "Fast realization of the modal vector fitting\nmethod for rational modeling with accurate representation of small eigenvalues", <em>IEEE Trans. Power Del.</em>, vol. 24, no. 3, pp. 1396-1405, Jul. 2009.', 'B. Gustavsen, "Fast passivity enforcement\nfor pole-residue models by perturbation of residue matrix eigenvalues", <em>IEEE Trans. Power Del.</em>, vol. 23, no. 4, pp. 2278-2285, Oct. 2008.', 'B. Gustavsen and A. Semlyen, "Enforcing passivity for admittance\nmatrices approximated byrational functions", <em>IEEE Trans. Power Syst.</em>, vol. 16, no. 1, pp. 97-104, Feb. 2001.', 'J. Mahseredjian, S. Dennetire, L. Dub, B. Khodabakhchian and L. Grin-Lajoie, "On a new approach for the simulation\nof transients in power systems", <em>Elect. Power Syst. Res.</em>, vol. 77, no. 11, pp. 1514-1520, Sep. 2007.', 'M. deMagistris and L. De Tommasi, "An identification technique\nfor macromodeling of long interconnects", <em>Proc. 9th IEEE Workshop Signal Propag. Interconnects</em>, pp. 185-188, 2005.', 'B. Gustavsen and A. Semlyen, "A robust approach for system\nidentification in the frequency domain", <em>IEEE Trans. Power Del.</em>, vol. 19, no. 3, pp. 1167-1173, Jul. 2004.', 'G. H. Golub and C. F. Van Loan, Matrix Computations, MD, Baltimore:Johns Hopkins Univ. Press, 1996.', 'Y. Hua and T. K. Sarkar, "On SVD for estimating generalized\neigenvalues of singular matrix pencil in noise", <em>IEEE Trans. Signal Process.</em>, vol. SP-39, no. 4, pp. 892-900, Apr. 1991.', 'Y. Hua and T. K. Sarkar, "Generalized pencil-of-function\nmethod for extracting poles of an EM system from its transient response", <em>IEEE Trans. Antennas Propag.</em>, vol. AP-37, no. 2, pp. 229-234, Feb. 1989.']
#
# parser = AnystyleParser()
# pa=[]
# for item in bib:
#     item = item.replace('\n', '')
#     item = item.replace('<em>', '')
#     item = item.replace('</em>', '')
#     print(item)
#     # item = remove_tags(item)
#     parsed = parser.parse_reference(item)
#     pa.append(parsed)
# print(pa)