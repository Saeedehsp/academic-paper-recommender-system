import bibtexparser

import requests
from bs4 import BeautifulSoup

session = requests.session()

def search_article(bibliographi):
    response = session.request("GET", 'https://scholar.google.com/scholar?', params={
        'hl': 'en',
        'as_sdt' : '0%2C5',
        'q' : bibliographi
    })
    if 'Please show you&#39;re not a robot' in response.text:
        raise RuntimeError('robot found')
    soup = BeautifulSoup(response.text, features="html5lib")
    # meta = soup.select_one('a[title=Cite]')
    id_class = soup.select_one('.gs_r.gs_or.gs_scl')
    id = id_class.attrs['data-cid']
    cite_response = session.request("GET", 'https://scholar.google.com/scholar?', params={
        'hl': 'en',
        'scirp' : '0',
        'output' : 'cite',
        'q' : 'info:'+id+':scholar.google.com/'
    })
    cite_soup = BeautifulSoup(cite_response.text, features="html5lib")
    cite_page = cite_soup.select_one('.gs_citi')
    href = cite_page.attrs['href']
    href_response = session.request("GET", href)
    bibtex = href_response.text
    bib_database = bibtexparser.loads(bibtex)
    if len(bib_database.entries) > 0:
        json = bib_database.entries[0]

    print(json)
