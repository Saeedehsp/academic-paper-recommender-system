from thirdparty_api.any_style import AnyStyle
# from thirdparty_api.citation_parser import ieee_citation_parser
from thirdparty_api.any_style_ruby import AnystyleParser
import html
import re

clean_html = re.compile('<.*?>')

def parse_bibliographic(bibliographic=None):
    """Parse bibliographics with Anystyle instance"""
    if bibliographic is None:
        bibliographic = []
    any_style = AnyStyle()
    return any_style.parse_bibliographic(bibliographic)
# #
# # text = '[1] Ian F Akyildiz, Weilian Su, Yogesh Sankarasubramaniam, and Erdal Cayirci. Wireless sensor networks: a survey. Computer networks, 38(4):393–422, 2002. [2] Guoqiang Mao, Barıs¸ Fidan, and Brian DO Anderson. Wireless sensor network localization techniques. Computer networks, 51(10):2529–2553, 2007. [3] Steven M Kay. Fundamentals of statistical signal processing. Prentice Hall PTR, 1993. [4] Mou Wu, Naixue Xiong, and Liansheng Tan. Adaptive range-based target localization using diffusion Gauss-Newton method in industrial environments. IEEE Transactions on Industrial Informatics, 2019. [5] Amir Beck, Petre Stoica, and Jian Li. Exact and approximate solutions of source localization problems. IEEE Transactions on signal processing, 56(5):1770–1778, 2008. [6] Slavisa Tomic, Marko Beko, and Rui Dinis. RSS-based localization in wireless sensor networks using convex relaxation: Noncooperative and cooperative schemes. IEEE Transactions on Vehicular Technology, 64(5):2037–2050, 2015. [7] Reza Monir Vaghefi, Mohammad Reza Gholami, R Michael Buehrer, and Erik G Strom. Cooperative received signal strength-based sensor localization with unknown transmit powers. IEEE Transactions on Signal Processing, 61(6):1389–1403, 2013. [8] Stephen Boyd and Lieven Vandenberghe. Convex optimization. Cambridge university press, 2004. [9] Yaming Xu, Jianguo Zhou, and Peng Zhang. RSS-based source localization when path-loss model parameters are unknown. IEEE communications letters, 18(6):1055–1058, 2014. [10] Gang Wang and Kehu Yang. A new approach to sensor node localization using RSS measurements in wireless sensor networks. IEEE transactions on wireless communications, 10(5):1389–1395, 2011. [11] Yongchang Hu and Geert Leus. Robust differential received signal strength-based localization. IEEE Transactions on Signal Processing, 65(12):3261–3276, 2017. [12] Danaee MR, and Behnia F. Improved Least Squares Approaches for Differential Received Signal Strength-Based Localization with Unknown Transmit Power. Wireless Personal Communications, 110(3):1373–1401, 2020.'
# text = "<p class=\"References\"><!--[if !supportLists]-->[1]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->R.C. Gonzalez, R.E. Woods, Digital Image Processing, 3rd Edition, Prentice Hall, 2007.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[2]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->R. Drigger, P. Cox, T. Edwards, Introduction to Infrared and Electro-Optical System, Artech House Publishers, 1999.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[3]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->R.A. Schowengerdt, Remote Sensing: Models and Methods for Image processing, 2nd Edition, Academic Press, 1997.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[4]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->V. Castelli, L.D. Bergman, Image Databases, Search and Retrieval of Digital Imagery, John Wiley &amp; Sons, 2002.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[5]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->T.Y. Ji, M.S. Li, Z. Lu, O.H. Wu, \"Optimal morphological filter design using a bacterial swarming algorithm,\" in Proc. 2008 IEEE Congress on Evolutionary Computation, pp. 452-458.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[6]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->T.Y. Ji, Z. Lu, O.H. Wu, \"Optimal soft morphological filter for periodic noise removal using a particle swarm optimiser with passive congregation,\" Signal Processing, Vol. 87, Issue 11, pp. 2799-2809, 2007.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[7]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->I. Aizenburg, C. Butakoff, \"A windowed Gaussian notch filter for quasi-periodic noise removal,\" Image and Vision Computing, Vol. 26, Issue 10, pp. 1347-1353.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[8]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->I. Aizenberg, C. Butakoff, \"Frequency Domain Median-like Filter for Periodic and Quasi-Periodic Noise Removal,\" SPIE Proceeding, Vol. 4667, 181-191, 2002.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[9]&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->I. Aizenberg, C. Butakoff, J. Astola, K. Egiazarian, \"Nonlinear Frequency Domain Filter for the Quasi-Periodic Noise Removal,\" in Proc. 2002 International TICSP Workshop on Spectral Methods and Multirate Signal Processing, pp. 147-153.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[10]&nbsp; <!--[endif]-->R. Venkateswarlu, K.V. Sujata, B. Venkateswara Rao, \"Centroid Tracker and Point Selection,\" SPIE Proceeding, Vol. 1697, pp. 520-529, 1992.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[11]&nbsp; <!--[endif]-->R. Szeliski, Computer Vision: Algorithms and Applications, Springer, 2010.</p>\r\n<p class=\"References\"><!--[if !supportLists]-->[12]&nbsp; <!--[endif]-->R.C. Gonzalez, R.E. Woods, S.L Eddins, Digital Image Processing using MATLAB, Gatesmark Publishing, 2nd Edition, 2009.</p>"
# text = [['\r', '1]\xa0\xa0\xa0\xa0 Rahmati A.; Abrishamifar A.; Abiri E.; “Space vector modulation direct power control in an inverter connected to grid and wind turbine”, In Persian, fourteenth electrical conference, Iran, May 2006.\r', '2]\xa0\xa0\xa0\xa0 Malinowski M.; “Sensorless control strategies for three-phase PWM\xa0 rectifiers”, Ph.D. dissertation, Inst. Control Ind. Electron., Warsaw Univ. Technol., Warsaw, Poland, 2001.\r', '3]\xa0\xa0\xa0\xa0 Noguchi T.; Tomiki H.; Kondo S.; TakahashiI.; “Direct power control of\xa0 PWM converter without power-source voltage sensors”, IEEE Trans. Ind. Applicat., vol.34, pp.473-479, May/June 1998.\r', '4]\xa0\xa0\xa0\xa0 Peng F., Z.; Ott G., W.; Adams D., J.; “Harmonic and reactive power compensation based on the generalized instantaneous reactive theory for three-phase four-wire systems”, IEEE Trans. Power Elec., vol.13, pp.1174-1181, Nov.1998.\r', '5]\xa0\xa0\xa0\xa0 Kwon B., H.; Lim J., H.; “A line-voltage-sensorless synchronous rectifier”, IEEE Trans. Ind. Applicant, vol.14, pp.966-972, sept.1999.\r', '6]\xa0\xa0\xa0\xa0 Chen S.; Joos G.; “Direct power control of active filters for voltage flicker mitigation”, Ind. Appl. Conference, Thirty-sixth IAS Annual Meeting, pp.2683-2690, September/October 2001.\r', '7]\xa0\xa0\xa0\xa0 Cichowias M.; Malinoaski M.; Kazmierkowski M., P.; Sobczuk D., L.; Rodrigues P.; Pou J.; “Active filtering function of three-phase PWM Boost rectifier under different line voltage conditions”, IEEE\xa0 Trans. On Ind. Elec., vol.52, no.2, pp.410-419, April 2005.\r', '8]\xa0\xa0\xa0\xa0 Ohnishi T.; “Three-phase PWM converter/inverter by means of instantaneous active and reactive power control”, IECON’91, pp.819-824, 1991.\r', '9]\xa0\xa0\xa0\xa0 Rodriguez P.; Bergas J.; Pou J.; Candela I.; Burgos R.; Boroyevich D.; “Double synchronous reference frame PLL for power converters control”, IEEE 36th. conf. on power electronic specialists, pp.1415-1421, 2005.\r\r\xa0\r\xa0']]


def remove_html_tags(text):
    """Remove html tags from a string"""
    text = re.sub(clean_html, '', text)
    text = html.unescape(text)
    text = text.replace('\n', '')
    return text

def remove_tags(text):
    text = text.replace('<em>', '')
    text = text.replace('</em>', '')
    text = text.replace('\n', '')
    return text

# text = remove_html_tags(text)
# print(text)
# tex = text.split('[')
# print(parse_bibliographic(tex))

# def parse_bibliographic(bibliographic=None):
#     """Parse bibliographics with Anystyle ruby instance"""
#     if bibliographic is None:
#         bibliographic = []
#     parsed_biblio = []
#     bibliographic = remove_html_tags(bibliographic)
#     parser = AnystyleParser()
#     # for item in bibliographic:
#     parsed = parser.parse_reference(bibliographic)
#     parsed_biblio.append(parsed)
#     print(parsed_biblio)
#     return parsed_biblio


