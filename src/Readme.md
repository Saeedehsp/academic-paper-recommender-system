Academic Paper Recommendation System
====================================
Recommends academic papers based on scholars' articles.

Requirements
------------
You need to have these requirements to be able to run the project:
- Python 3: https://www.python.org/downloads/
- PIP: https://pypi.org/project/pip/#downloads
- MongoDB: https://www.mongodb.com/try/download/community
- Docker: https://docs.docker.com/get-docker/

How to Run
----------
To run the project, type this in command line:
```shell script
docker-compose up --build
```
URL: http://localhost:8000
