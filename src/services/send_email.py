import smtplib
import pymongo, pprint
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import config

# Email username and password
MY_ADDRESS = 'eej_recommender@outlook.com'
PASSWORD = 'auteej2020'

myclient = pymongo.MongoClient(config.database_uri)

mydb = myclient["recommender"]
contacts = mydb["contacts"]


def get_contacts(user_id):
    """Gets scholar contact info from database"""
    names = []
    emails = []
    scholar_contacts = contacts.find({'user_id': user_id})
    for a_contact in scholar_contacts:
        names.append(a_contact['name'])
        emails.append(a_contact['email'])
    return names, emails


def read_template(filename):
    """
    Returns a Template object comprising the contents of the
    file specified by filename.
    """

    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


def send_email(articles, contact_id):
    articles = list(set(articles))
    names, emails = get_contacts(contact_id)  # read contacts
    template_file = open('../message.txt', 'w', encoding='utf-8')
    s = smtplib.SMTP(host='smtp-mail.outlook.com', port=587)
    s.starttls()
    print('logggi')
    s.login(MY_ADDRESS, PASSWORD)

    # For each contact, send the email:
    for name, email in zip(names, emails):
        msg = MIMEMultipart()  # create a message
        for num,item in enumerate(articles, start=1):
            template_file.write('{} - {}, \n'.format(num,item))
        # Prints out the message body for our sake
        template_file = open('../message.txt', 'r', encoding='utf-8')
        template_file_content = template_file.read()
        mess = template_file_content
        print(mess)
        # setup the parameters of the message
        msg['From'] = MY_ADDRESS
        msg['To'] = email
        msg['Subject'] = "EEJ recommneder"

        # add in the message body
        msg.attach(MIMEText(mess, 'plain'))

        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg

    # Terminate the SMTP session and close the connection
    s.quit()


def send_email_directly(articles, email):
    print("sendiiiiiiiiiiing")
    articles_str = ''.join(articles)
    template_file = open('../message.txt', 'w', encoding='utf-8')
    s = smtplib.SMTP(host='smtp-mail.outlook.com', port=587)
    s.starttls()
    s.login(MY_ADDRESS, PASSWORD)
    msg = MIMEMultipart()  # create a message
    template_file.write('{}'.format(articles_str))
    template_file = open('../message.txt', 'r', encoding='utf-8')
    template_file_content = template_file.read()
    mess = template_file_content
    print(mess)
    # setup the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = email
    msg['Subject'] = "EEJ recommneder"

    # add in the message body
    msg.attach(MIMEText(mess, 'plain'))

    # send the message via the server set up earlier.
    s.send_message(msg)
    print("seeeeeeeent")
    del msg

    # Terminate the SMTP session and close the connection
    s.quit()



