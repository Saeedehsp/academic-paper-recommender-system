# coding: utf-8
import ssl
import pprint
import pymongo
from fuzzywuzzy import fuzz
import config
from parsers.bibliographic import remove_tags
from services.send_email import send_email

myclient = pymongo.MongoClient(config.database_uri)

mydb = myclient["recommender"]
ieee = mydb["ieee"]
journal = mydb["eejjournal"]
scholar = mydb["scholars"]
contacts = mydb["contacts"]


def compare_two_articles_by_citations(article1, article2):
    """Compare citations with more than 80% similarity"""
    scholar_ref = article1["citation"]
    jour_ref = article2["citation"]

    title_similarity_rate = fuzz.ratio(article1['title'], article2['title'])
    if title_similarity_rate < 90:
        shared_citations = []
        if scholar_ref:
            for i in range(0, len(scholar_ref)):
                for j in range(0, len(jour_ref)):
                    if scholar_ref[i] and jour_ref[j]:
                        if "title" not in scholar_ref[i]:
                            continue
                        if "title" not in jour_ref[j]:
                            continue
                        similarity_rate = fuzz.ratio(scholar_ref[i]["title"], jour_ref[j]["title"])

                        if similarity_rate > 80:
                            authors = ''
                            if 'author' in jour_ref[j]:
                                for author in jour_ref[j]['author']:
                                    if 'family' in author and 'given' in author:
                                        authors = authors + author['family'] + ', ' + author['given'] + ', '
                            else:
                                authors = ""

                            if 'volume' in jour_ref[j]:
                                volume = jour_ref[j]['volume']
                            else:
                                volume = ""

                            if 'issue' in jour_ref[j]:
                                issue = "(" + jour_ref[j]['issue'] + ")"
                            else:
                                issue = ""

                            if 'page' in jour_ref[j]:
                                page = jour_ref[j]['page']
                            else:
                                page = ""

                            if 'container-title' in jour_ref[j]:
                                container = jour_ref[j]['container-title']
                                container = remove_tags(container)
                            else:
                                container = ""

                            if 'publisher' in jour_ref[j]:
                                publisher = jour_ref[j]['publisher']
                                publisher = remove_tags(publisher)
                            else:
                                publisher = ""

                            if 'issued' in jour_ref[j] and 'date-parts' in jour_ref[j]['issued']:
                                date = "(" + str(jour_ref[j]['issued']['date-parts'][0][0]) + ")."
                            else:
                                date = ""

                            title = ''
                            citation_title = jour_ref[j]['title'].title()
                            if citation_title:
                                if container and volume and issue and page and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + volume + issue + ', pp. ' + page

                                elif container and volume and issue and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + volume + issue

                                elif container and volume and page and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + volume + ', pp. ' + page

                                elif container and issue and page and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + issue + ', pp. ' + page

                                elif volume and issue and page and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher + ', ' + issue + ', pp. ' + page

                                elif container and volume and issue:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + volume + issue

                                elif container and volume and page:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + volume + ', pp. ' + page

                                elif container and issue and page:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + issue + ', pp. ' + page

                                elif volume and issue and page:
                                    title = authors + date + ' ' + citation_title + ', ' + issue + ', pp. ' + page

                                elif container and issue and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + issue

                                elif container and volume and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + publisher + ', ' + volume

                                elif volume and issue and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher + ', ' + volume + issue

                                elif container and issue:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + issue

                                elif container and volume:
                                    title = authors + date + ' ' + citation_title + ', ' + container + ', ' + volume

                                elif volume and issue:
                                    title = authors + date + ' ' + citation_title + ', ' + volume + issue

                                elif volume and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher + ', ' + volume

                                elif issue and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher + ', ' + issue

                                elif container and publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher + ', ' + container

                                elif volume:
                                    title = authors + date + ' ' + citation_title + ', ' + volume

                                elif issue:
                                    title = authors + date + ' ' + citation_title + ', ' + issue

                                elif container:
                                    title = authors + date + ' ' + citation_title + ', ' + container

                                elif publisher:
                                    title = authors + date + ' ' + citation_title + ', ' + publisher

                                else:
                                    title = authors + date + ' ' + citation_title

                            if jour_ref[j]['title'] != '':
                                shared_citations.append(title)

        return shared_citations


def compare_two_articles_by_keywords(article1, article2):
    """Compare keywords with more than 80% similarity"""
    ieee_keywords = article1["keywords"]
    jour_keywords = article2["keywords"]
    title_similarity_rate = fuzz.ratio(article1['title'], article2['title'])
    if title_similarity_rate < 90:
        shared_keywords = []
        if ieee_keywords:
            for i in range(0, len(ieee_keywords)):
                for j in range(0, len(jour_keywords)):
                    if ieee_keywords[i] is None:
                        continue
                    if jour_keywords[j] is None:
                        continue
                    similarity_rate = fuzz.ratio(ieee_keywords[i], jour_keywords[j])
                    if similarity_rate > 90:
                        shared_keywords.append(ieee_keywords[i])
        return shared_keywords


def compare_scholar_with_one_paper(scholars, journal_article, parameters):
    """Compare all scholar's paper with one journal and check parameters"""
    reference_param = parameters['referenceParam']
    keyword_param = parameters['keywordParam']
    shared_articles_by_ref = []
    shared_articles_by_kw = []
    scholar_articles = scholars['articles']
    for article in scholar_articles:
        shared_citations = compare_two_articles_by_citations(article, journal_article["article"])
        if shared_citations:
            if len(shared_citations) >= int(reference_param):
                shared_articles_by_ref.append({
                    'The paper published by you': article['title'],
                    'Suggested papers published in AJEE': journal_article["article"]["title"],
                    'Common references': shared_citations
                })
        shared_keywords = compare_two_articles_by_keywords(article, journal_article["article"])
        if shared_keywords:
            if len(shared_keywords) >= int(keyword_param):
                shared_articles_by_kw.append({
                    'The paper published by you': article['title'],
                    'Suggested papers published in AJEE': journal_article['article']['title'],
                    'Common keywords': shared_keywords
                })

    shared_articles_by_ref = {each['Suggested papers published in AJEE']: each for each in
                              shared_articles_by_ref}.values()
    shared_articles_by_kw = {each['Suggested papers published in AJEE']: each for each in
                             shared_articles_by_kw}.values()

    return shared_articles_by_ref, shared_articles_by_kw


def find_best_articles(user_id, parameters):
    """Find similar articles by comparing all journal articles with scholar articles"""
    myscholar = scholar.find_one({"user_id": user_id})
    shared_articles = []
    shered_by_ref = []
    shered_by_kw = []
    # jour_articles = journal.find(no_cursor_timeout=True)
    jour_articles = journal.find()
    for jour_article in jour_articles:
        shered_by_ref, shered_by_kw = compare_scholar_with_one_paper(myscholar, jour_article, parameters)
        shared_articles.extend(shered_by_ref)
        shared_articles.extend(shered_by_kw)
    return shared_articles


def compare_one_article_with_one_paper(info, journal_article, parameters):
    """Compare one article with one journal article"""
    reference_param = parameters['referenceParam']
    keyword_param = parameters['keywordParam']
    shared_articles_by_ref = []
    shared_articles_by_kw = []
    one_article = info
    shared_citations = compare_two_articles_by_citations(one_article, journal_article["article"])
    if shared_citations:
        if len(shared_citations) >= int(reference_param):
            if one_article['title'] != journal_article['article']['title']:
                shared_articles_by_ref.append({
                    'The paper published by you': one_article['title'],
                    'Suggested papers published in AJEE': journal_article["article"]["title"],
                    'Common references': shared_citations
                })
    shared_keywords = compare_two_articles_by_keywords(one_article, journal_article["article"])
    if shared_keywords:
        if len(shared_keywords) >= int(keyword_param):
            if one_article['title'] != journal_article['article']['title']:
                shared_articles_by_kw.append({
                    'The paper published by you': one_article['title'],
                    'Suggested papers published in AJEE': journal_article['article']['title'],
                    'Common keywords': shared_keywords
                })
    return shared_articles_by_ref, shared_articles_by_kw


def find_best_articles_for_one(info, parameters):
    """Find similar articles by comparing all journal articles with one journal article"""
    shared_articles = []
    shered_by_ref = []
    shered_by_kw = []
    jour_articles = journal.find()
    for jour_article in jour_articles:
        shered_by_ref, shered_by_kw = compare_one_article_with_one_paper(info, jour_article, parameters)
        shared_articles.extend(shered_by_ref)
        shared_articles.extend(shered_by_kw)
    return shared_articles
