from pymongo import MongoClient
import ssl

# Database model class and functions

class Model:
    def __init__(self, mongodb_uri):
        client = MongoClient(mongodb_uri)
        self.db = client.recommender

    def insert_ieee_article(self, article):
        self.db.ieee.insert_one(article)

    def insert_journal(self, DOI, article):
        self.db.eejjournal.update_one(filter={"DOI": DOI}, update={'$set': {'article': article}}, upsert=True)

    def update_journal(self, DOI, article):
        self.db.journal.update_one(filter={"DOI": DOI}, update={'$set': {'article': article}}, upsert=True)

    def insert_scholar_article(self, scholar):
        self.db.scholars.insert_one(scholar)

    def update_scholar_article(self, user_id, articles, total_article_count):
        self.db.scholars.update_one(filter={"user_id": user_id}, update={'$set': {'articles': articles, 'total_article_count': total_article_count}},
                                    upsert=True)

    def update_scholar_contact(self, user_id, name, email):
        self.db.contacts.update_one(filter={"user_id": user_id}, update={'$set': {'email': email, 'name': name}},
                                    upsert=True)

    def insert_scholar_email(self, email):
        self.db.recommendations.insert_one(email)

    def update_scholar_email(self, user_id, res):
        self.db.recommendations.update_one(filter={"user_id": user_id}, update={'$set': {'recommendations': res}},
                                          upsert=True)

    def update_solo_email(self, email, res):
        self.db.solorecommendation.update_one(filter={"email": email}, update={'$set': {'recommendations': res}},
                                          upsert=True)

    def get_email_text(self, user_id):
        res = self.db.recommendations.find_one({"user_id": user_id})
        print(res)
        return res

    def count_journal_entry(self):
        return self.db.eejjournal.count()

    def check_scholar_exists(self, user_id):
        exist = False
        res = self.db.scholars.find_one({"user_id": user_id})
        if res is not None:
            exist = True
        return exist

    def scholar_articles_size(self, user_id):
        scholars = self.db.scholars.find_one({"user_id": user_id})
        leng = scholars['total_article_count']
        print(leng)
        return leng
