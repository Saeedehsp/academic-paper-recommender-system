import requests
from bs4 import BeautifulSoup
from parsers.bibliographic import parse_bibliographic, remove_html_tags


def get_springer_info(link):
    """Get an Springer article references and keywords"""
    response = requests.request("GET", link)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    soup = BeautifulSoup(response.text, features="html5lib")
    refs = []
    article_keywords = []
    title = ''
    # some links have article in path
    if '/article' in link:
        reference = soup.select_one('.c-article-references')
        if reference is not None:
            for item in reference.contents:
                ref_text = item.select_one('.c-article-references__text')
                ref = remove_html_tags(ref_text.text)
                refs.append(ref)
        # get keywords
        keywords = soup.select_one('.c-article-subject-list')
        if keywords is not None:
            for item in keywords.contents:
                key_text = item.contents[0].text
                article_keywords.append(key_text)

        # get references
        title_class = soup.select_one('.c-bibliographic-information__citation')
        if title_class is not None:
            text = title_class.text.replace('\n', '')
            text = text.replace('                   ', '')
            title = remove_html_tags(text)

    # some links have chapter in path
    elif '/chapter' in link:
        reference = soup.select_one('.BibliographyWrapper')
        if reference is not None:
            for item in reference.contents:
                ref_text = item.select_one('.CitationContent')
                refs.append(ref_text.text)

        # get keywords
        keywords = soup.select_one('.KeywordGroup')
        if keywords is not None:
            for item in keywords.contents:
                if item.name != 'h2':
                    keywords_text = item.text
                    if keywords_text.endswith('\xa0'):
                        keywords_text = ''.join(keywords_text.split())
                    article_keywords.append(keywords_text)

        # get references
        title_class = soup.select_one('#citethis-text')
        if title_class is not None:
            text = title_class.text.replace('\n', '')
            text = text.replace('                   ', '')
            title = remove_html_tags(text)

    parsed_ref = parse_bibliographic(refs)
    if parsed_ref:
        for i in range(0, len(parsed_ref)):
            if parsed_ref[i]:
                if 'title' in parsed_ref[i]:
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

    return {
        'title': title if title else "",
        'citation': parsed_ref,
        'keywords': article_keywords,
    }
