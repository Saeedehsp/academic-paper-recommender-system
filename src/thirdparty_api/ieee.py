import requests
from bs4 import BeautifulSoup
from parsers.bibliographic import parse_bibliographic, remove_html_tags


def get_ieee_info(link):
    """Get an IEEE article references and keywords"""
    identity = link.rstrip('/').split('/')[-1]

    # get references
    url = "https://ieeexplore.ieee.org/rest/document/" + str(identity) + "/references"
    headers = {'referer': "https://ieeexplore.ieee.org/abstract/document/" + str(identity) + "/references"}
    response = requests.request("GET", url, headers=headers)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    json_response = response.json()
    parsed_ref = []
    bibs = []
    if 'references' in json_response:
        refs = [ref['text'] for ref in json_response['references']]

        for ref in refs:
            ref = remove_html_tags(ref)
            bibs.append(ref)
        parsed_ref = parse_bibliographic(bibs)
        for i in range(0, len(parsed_ref)):
            if parsed_ref[i]:
                if 'title' in parsed_ref[i]:
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

    # get keywords
    url = "https://ieeexplore.ieee.org/rest/document/" + str(identity) + "/keywords"
    headers = {'referer': "https://ieeexplore.ieee.org/abstract/document/" + str(identity) + "/keywords"}
    response = requests.request("GET", url, headers=headers)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    json_response = response.json()
    article_keywords = []
    if 'keywords' in json_response and len(json_response['keywords']) > 0 and 'kwd' in json_response['keywords'][0]:
        ieee_keywords = json_response['keywords'][0]['kwd']
        for keyword in ieee_keywords:
            article_keywords.append(keyword)

    url_title = "https://ieeexplore.ieee.org/rest/search/citation/format?recordIds=" + identity + "&download-format=download-ascii&lite=true"
    headers2 = {'referer': "https://ieeexplore.ieee.org/abstract/document/" + str(identity)}
    response2 = requests.request("GET", url_title, headers=headers2)
    response2 = response2.json()
    title = remove_html_tags(response2['data'])

    return {
        'title': title,
        # 'DOI': doi,
        'citation': parsed_ref,
        'keywords': article_keywords,
        'url': 'https://ieeexplore.ieee.org/document/' + str(identity)
    }


