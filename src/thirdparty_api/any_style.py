import json

import requests
from bs4 import BeautifulSoup

"""Anystyle website reference parsing class"""

class AnyStyle:
    def __init__(self):
        self.session = requests.session()

    def get_csrf(self):
        response = self.session.request("GET", 'https://anystyle.io/')
        soup = BeautifulSoup(response.text, features="html5lib")
        meta = soup.select_one('meta[name=csrf-token]')
        if meta is not None and "content" in meta.attrs:
            return meta.attrs["content"]
        else:
            raise RuntimeError("Could not extract csrf")

    def parse_bibliographic(self, citations=None):
        if citations is None:
            citations = []
        csrf = self.get_csrf()
        data = {
            'input': citations
        }
        headers = {
            'Content-type': 'application/json',
            'Accept': 'application/json, text/plain, */*',
            'origin': 'https://anystyle.io',
            'referer': 'https://anystyle.io/',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
            'x-csrf-token': csrf
        }
        response = self.session.request("POST", 'https://anystyle.io/parse?format=csl',
                                        data=json.dumps(data), headers=headers)
        if response.status_code != 200:
            raise RuntimeError('HTTP error : ' + response.reason)
        return response.json()
