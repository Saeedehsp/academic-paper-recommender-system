# -*- coding: utf-8 -*-
from parsers.bibliographic import remove_html_tags, parse_bibliographic
from thirdparty_api.journal_api import get_one_journal_articles


def get_one_article_info(title,ref, keys):
    # refer = remove_html_tags(ref)
    refer = []
    print("reeeeeeeefs",ref)
    print("keeeeeeeys", keys)
    refer.append(ref)
    ref = ref.split('[')
    # print('fdfdfdfdfd',ref)
    parsed_ref = parse_bibliographic(ref)
    # print("parseddddddd: ", parsed_ref)
    for i in range(0, len(parsed_ref)):
        if parsed_ref[i]:
            if 'title' in parsed_ref[i]:
                parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

    keywords = keys.split(";")

    article_info = {
        'title': title,
        'citation': parsed_ref,
        'keywords': keywords,
    }

    return article_info

# print(get_one_article_info("","[1] M. Papageorgiou, J. Blosseviller, and H. Hadj-Salem, “Modelling and real-time control of traffic flow on the southern part of boulevard peripherique in paris: Part i: modelling”, Transportation Research, vol. 24A, no. 5, pp. 345– 359, 1990. [2] Y. Wang, M. Papageorgiou, and A. Messmer, “A real-time freeway network traffic surveillance tool”, IEEE Trans. Control Systems Technology, vol. 14, no. 1, pp. 18– 32, 2006. [3] L. Ljung., “System identification, Theory for the user. System sciences series”, Prentice Hall, Upper Saddle River, NJ, USA, second edition, 1999. [4] T. Soderstrom and P. Stoica, “System identification. Systems and Control Engineering”, Prentice Hall, 1989. [5] Doucet, A., de Freitas, J.F.G. and Gordon N.J. (eds.), “Sequential Monte Carlo Methods in Practice”, New York: Springer- Verlag, 2001. [6] Guyader, A., LeGland, F. and Oudjane, N., “A particle implementation of the recursive MLE for partially observed diffusions”, Proceedings of the 13th IFAC Symposium on System Identfication, 1305- 1310, 2003. [7] Cerou F., LeGland F. and Newton N.J., “Stochastic particle methods for linear tangent equations in Optimal Control and PDE's- Innovations and Applications”, eds. J. Menaldi, E. Rofman, 2001.",""))