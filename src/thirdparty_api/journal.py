from bs4 import BeautifulSoup
import requests


def get_journal_article(article_id):
    url = "https://eej.aut.ac.ir/article_{}.html".format(article_id)
    response = requests.request("GET", url)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    text_response = response.text

    soup = BeautifulSoup(text_response, features="html5lib")
    ref = soup.select('#ar_ref_full p.References')

    # article keywords
    keywords = []
    keywords_tag = soup.select('meta[name="keywords"]')
    if keywords_tag is not None and len(keywords_tag) > 0 and 'content' in keywords_tag[0]:
        key = keywords_tag[0]['content']
        keywords = key.split(',')

    cite = ''
    to_cite = soup.select('#cite_harvard')
    if to_cite is not None and len(to_cite) > 0:
        to_cite = to_cite[0].next.text
        to_cite = to_cite.replace("'", "")
        cite = to_cite

    # Sometimes there are problems in AUT Journal tags, when there is no reference,
    #     # it will try to fix those tags and try again
    if ref is not None and len(ref) == 0:
        ref = BeautifulSoup(text_response.replace("</p>", ""), features="html5lib").select('#ar_ref_full div')

    refs = []

    # Citations have index, it excludes the index part and the space after that
    if ref is not None and len(ref) > 0:
        for item in ref:
            refs.append(item.text)
            # parts = item.text.replace('\xa0', '').split(' ', 1)
            # if len(parts) > 1:
            #     refs.append(parts[1])
    tit = soup.select('h1.citation_title')
    title = ''
    if tit is not None and len(tit) > 0:
        title = tit[0].text
    link = soup.select('span#ar_doi a')
    doi = ''
    if link is not None and len(link) > 0:
        doi = link[0].text

    if cite == '' and doi == '' and len(refs) == 0:
        return None

    return {
        'title': cite,
        'DOI': doi,
        'citation': refs,
        'keywords': keywords,
        'url': url
    }

