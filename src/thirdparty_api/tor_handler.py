import socket
import time

from stem import Signal
from stem.control import Controller


def renew_connection():
    """Generate new IP by signaling tor proxy"""
    try:
        with Controller.from_port(address=socket.gethostbyname('tor-privoxy'), port=9051) as controller:
            controller.authenticate()
            controller.signal(Signal.NEWNYM)
            controller.close()
            time.sleep(20)
    except:
        pass
