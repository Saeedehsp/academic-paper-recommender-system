from thirdparty_api.scholar import get_scholar_articles
import collections

def get_publishers(user_id):
    """Find most common publishers"""
    articles = []
    try:
        articles = get_scholar_articles(user_id)
    except:
        print("could not get scholar articles")

    publishers = []
    for article in articles:
        if 'link' not in article:
            print("there is not link in article, ignoring")
            continue
        url = article['link']
        publisher = url.split('/')[2]
        publishers.append(publisher)
    return publishers


def sort_publishers(user_id):
    publishers = get_publishers(user_id)
    counter = collections.Counter(publishers)
    print(counter.most_common(20))

