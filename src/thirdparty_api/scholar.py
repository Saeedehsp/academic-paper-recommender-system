import config
from bs4 import BeautifulSoup
import requests

# TODO: Extract user_id with name or some UI
# TODO: Handle next pages
from thirdparty_api.tor_handler import renew_connection

# Google request headers
headers = {'Cookie' : 'NID=208=TM4uJQE5PczRxonmX51q9FmpAvEaUuoaOp-fBiChEO8--YA9iVq_23FbrHefU-RmZiTMwrOtKW7e-tUL05J-V5xdO0UMl8cUoWBAajaBi-hsiwrqT_sOFi6_Owv2_3q1MCm9QtDNWTtCdD_ng-V5oF9QbEv3v0-C2kSQj4CBDAWTSZNGBOGVKGfqqlWO7zhmDT2CtYIStrI-jKTw5yrZyWL6ZeBlF2LKjV4LtNdXgVg2nVk3RSIpgoQ_Ad32fJGznHPuQni4sf1uBqFB9W9m9w',
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}
proxies = {'http': 'http://tor-privoxy:8118'}


def get_articles_number(user_id):
    """Request scholar's page to get scholar's number of articles"""
    responseip = requests.request('GET', 'http://icanhazip.com')
                                                     # ,proxies=proxies)
    print(responseip.text)
    url = 'https://scholar.google.com/citations?hl=en&user={}&view_op=list_works'.format(user_id)
    response = requests.request('GET', url, headers=headers, proxies=proxies)

    while response.status_code == 429:
        renew_connection()
        response = requests.request('GET', url, headers=headers, proxies=proxies)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)

    print("Getting scholar articles list...")
    articles = show_more(user_id)
    print("Got {} articles".format(len(articles)))
    all_articles_count = len(articles)
    return all_articles_count


def get_scholar_articles(user_id):
    """Request scholar's page to get a list of all scholar's articles with it's name and link to publisher"""
    responseip = requests.request('GET', 'http://icanhazip.com', proxies=proxies)
    print(responseip.text)
    url = 'https://scholar.google.com/citations?hl=en&user={}&view_op=list_works'.format(user_id)
    response = requests.request('GET', url, headers=headers, proxies=proxies)

    while response.status_code == 429:
        renew_connection()
        response = requests.request('GET', url, headers=headers, proxies=proxies)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)

    print("Getting scholar articles list...")
    articles = show_more(user_id)
    print("Got {} articles".format(len(articles)))

    try:
        all_articles_count = len(articles)
        print('all articles count', all_articles_count)
    except:
       print("could not get scholar articles count")

    result = []
    if articles is not None:
        print("Getting scholar articles...")
        # p = io.StringIO("")p
        # for article in tqdm(articles, file=sys.stdout):
        if user_id not in config.context_dict:
            config.context_dict[user_id] = {'stop': False, 'prog': 0,
                                            'count': 0, 'spin': False, 'saved_in_database': False}

        for article in articles:
            print(article)
            if not config.context_dict[user_id]['stop']:
                if 'href' not in article.attrs:
                    print("'href' in not in article.attrs")
                    continue
                href = article.attrs['href']
                print(href)
                href_response = requests.request('GET', 'https://scholar.google.com' + href, headers=headers,proxies=proxies)
                if response.status_code == 429:
                    renew_connection()
                    href_response = requests.request('GET', 'https://scholar.google.com' + href, headers=headers,proxies=proxies)
                if response.status_code != 200:
                    print('HTTP error : ' + response.reason)
                    continue
                href_soup = BeautifulSoup(href_response.text, features='html5lib')
                # link_element = href_soup.select_one('.gsc_a_t')
                # if link_element is not None and 'href' in link_element.attrs:
                #     print(link_element)
                #     link = link_element.attrs['href']
                result.append({
                    'text': article.text,
                    'link': href
                })
                print(result)
                config.context_dict[user_id]['prog'] = (config.context_dict[user_id]['count']/len(articles))*80
                print(config.context_dict[user_id]['prog'])
                config.context_dict[user_id]['count'] = config.context_dict[user_id]['count']+1
    return result, all_articles_count


def show_more(user_id):
    """Request scholar's page to crawl every page of scholar's articles"""
    count = 0
    articles = []
    for i in range(100):
        url = "https://scholar.google.com/citations?user={}&hl=en&cstart={}&pagesize=80".format(user_id, count)
        myobj = {'json': '1'}
        response = requests.post(url, data=myobj, headers=headers, proxies=proxies)
        if response.status_code != 200:
            print('HTTP error : ' + response.reason)
            continue
        x = response.json()
        if 'B' in x:
            soup = BeautifulSoup(x['B'], features='html5lib')
            items = soup.select('.gsc_a_at')
            if items is not None:
                count += len(items)
                articles.extend(items)
        # if 'P' in x and x['P'] == 1:
        if 'N' not in x:
            break
    return articles

#print(get_scholar_articles("T2SSqfYAAAAJ"))
