from bs4 import BeautifulSoup
import requests
from tqdm import tqdm
import config
from database.model import Model
from parsers.bibliographic import parse_bibliographic, remove_html_tags
model = Model(config.database_uri)
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
}
def get_journal_articles():
    """Extract AUT EE Journal articles with details by sending requests to AUTEEJ webservices"""
    response = requests.request('GET', 'https://eej.aut.ac.ir/service?ar_list', headers=headers,
                                allow_redirects=False)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    data = response.json()

    for article in tqdm(data['articles']):
        if 'articleCode' not in article or 'articleAuthors' not in article or 'publishYear' not in article or 'articleTitle' not in article or 'articleVolume' not in article or 'articleIssue' not in article or 'startPage' not in article or 'endPage' not in article or 'doi' not in article or 'articleKeywords' not in article or 'articleLink' not in article:
            print("article data is incomplete, ignoring")
            continue
        try:
            article_code = article['articleCode']
            article_response = requests.request('GET', 'https://eej.aut.ac.ir/service?article_code=' + article_code,
                                                headers=headers, allow_redirects=False)
            if article_response.status_code != 200:
                raise RuntimeError('HTTP error : ' + article_response.reason)
            article_response = article_response.json()
            if 'references' not in article_response['articles']:
                raise RuntimeError('article does not have references')
            refer = article_response['articles']['references']
            refer = remove_html_tags(refer)
            refer = refer.split('[')
            parsed_ref = parse_bibliographic(refer)
            for i in range(0, len(parsed_ref)):
                if parsed_ref[i]:
                    if 'title' in parsed_ref[i]:
                        parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                        parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

            authors = ''
            for author in article['articleAuthors']:
                authors = authors + author['last_name'] + ', ' + author['first_name'][0] + '.' + ', '

            title = authors + '(' + article['publishYear'] + '). ' + article['articleTitle'] + ', ' + article['articleVolume'] + '(' + article['articleIssue'] + '), pp. ' + article['startPage'] + '-' + article['endPage'] + '. doi: ' + article['doi']
            model.insert_journal(article['doi'], {
                'title': title,
                'citation': parsed_ref,
                'keywords': article['articleKeywords'],
                'url': article['articleLink']
            })
        except:
            print("an error happened when getting article")

import re
url = 'abcdc.com'
url = re.sub('\.com$', '', url)

def get_one_journal_articles(link):
    """Extract one specific artcile from AUT EE Journal with details by sending requests to AUTEEJ webservices"""
    identity = link.rstrip('/').split('/')[-1]
    identity = re.sub('article_|.html', '', identity)
    references = []
    article_code = identity
    article_response = requests.request('GET', 'https://eej.aut.ac.ir/service?article_code=' + article_code,
                                        headers=headers, allow_redirects=False)
    if article_response.status_code != 200:
        raise RuntimeError('HTTP error : ' + article_response.reason)
    print(article_response)
    article_response = article_response.json()
    print(article_response)
    article = article_response['articles']['0']
    if 'references' not in article_response['articles']:
        raise RuntimeError('article does not have references')
    refer = article_response['articles']['references']
    refer = remove_html_tags(refer)
    references.append(refer)
    parsed_ref = parse_bibliographic(references)
    for i in range(0, len(parsed_ref)):
        if parsed_ref[i]:
            if 'title' in parsed_ref[i]:
                parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

    keywords = []
    for item in article['articleKeywords']:
        keywords.append(item['kw_name'])

    authors = ''
    for author in article['articleAuthors']:
        authors = authors + author['last_name'] + ', ' + author['first_name'][0] + '.' + ', '

    title = authors + '(' + article['publishYear'] + '). ' + article['articleTitle'] + ', ' + \
            article[ 'articleVolume'] + '(' + article['articleIssue'] + '), pp. ' + article['startPage'] + '-' + \
            article['endPage']

    article_info = {
        'title': title,
        'citation': parsed_ref,
        'keywords': keywords,
    }

    return article_info
