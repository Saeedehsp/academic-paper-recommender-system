import logging

from bibtexparser.bparser import BibTexParser

from doab import const
from doab.parsing_common import SubprocessMixin, CleanReferenceMixin
"""Anystyle package reference parsing methods"""

logger = logging.getLogger(__name__)


def parse_bibtex(reference, bibtex_parser=None):
    """Parse referneces by bibtex parser"""
    if bibtex_parser is None:
        bibtex_parser = BibTexParser()
        result_rr = bibtex_parser.parse(reference).get_entry_list()
        if result_rr is not None and len(result_rr) > 0:
            return result_rr[-1]
    # unable to parse
    return None


class BaseReferenceParser(CleanReferenceMixin):
    """ A base class for implementing reference parsers
    ChildrenMustImplement
        parse_reference()
    :param book_path: (str) The path to the book to be parsed
    """
    # a variable that children can override to specify how good they are
    # compared to other parsers
    accuracy = 0

    @classmethod
    def parse_reference(cls, reference):
        return None


class AnystyleParser(BaseReferenceParser, SubprocessMixin):
    """Anystyle parser package class"""
    accuracy = 60  # Works better than Cermine for non-english references
    NAME = const.ANYSTYLE
    CMD = "anystyle"
    ARGS = ["-f", "bib", "parse_reference"]

    @classmethod
    def parse_reference(cls, reference, bibtex_parser=None):
        """Calls cmd line methods to parse references"""
        if reference.startswith("-"):
            reference = f'"{reference}"'
        bibtex_reference = cls.call_cmd(reference)
        logger.debug(f"Bibtex {bibtex_reference}")
        result = parse_bibtex(bibtex_reference)
        fail_message = f'{cls.NAME} was unable to pull a title from {reference}'

        if not result or 'title' not in result or not result["title"]:
            logger.debug(fail_message)
            return None

        return result
