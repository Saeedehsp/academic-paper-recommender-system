from tqdm import tqdm
import config
from thirdparty_api.ACM import get_ACM_info
from thirdparty_api.ScienceDirect import get_scienceDirect_info
from thirdparty_api.ieee import get_ieee_info
from thirdparty_api.scholar import get_scholar_articles
from thirdparty_api.springer import get_springer_info


def get_ieee_articles(scholar_articles, user_id):
    """Filter articles published in IEEE from scholar articles"""
    # if not config.context_dict[user_id]['stop']:
    ieee_articles = list(filter(lambda article: ('ieee.org' in article['link']), scholar_articles))
    if len(ieee_articles) > 0:
        print("Getting ieee articles...")
        ieee_articles = list(map(lambda article: get_ieee_info(article['link']), tqdm(ieee_articles)))
    return ieee_articles


def get_springer_articles(scholar_articles, user_id):
    """Filter articles published in Springer from scholar articles"""
    # if not config.context_dict[user_id]['stop']:
    springer_articles = list(filter(lambda article: ('springer.com' in article['link']), scholar_articles))
    if len(springer_articles) > 0:
        print("Getting springer articles...")
        springer_articles = list(
            map(lambda article: get_springer_info(article['link']), tqdm(springer_articles)))
    return springer_articles


def get_ACM_articles(scholar_articles, user_id):
    """Filter articles published in ACM from scholar articles"""
    # if not config.context_dict[user_id]['stop']:
    acm_articles = list(filter(lambda article: ('acm.org' in article['link']), scholar_articles))
    if len(acm_articles) > 0:
        print("Getting ACM articles...")
        acm_articles = list(
            map(lambda article: get_ACM_info(article['link']), tqdm(acm_articles)))
    return acm_articles


def get_scienceDirect_articles(scholar_articles, user_id):
    """Filter articles published in Science Direct from scholar articles"""
    # if not config.context_dict[user_id]['stop']:
    scienceDirect_articles = list(
        filter(lambda article: ('sciencedirect.com' in article['link']), scholar_articles)
    )
    if len(scienceDirect_articles) > 0:
        print("Getting scienceDirect articles...")
        scienceDirect_articles = list(
            map(lambda article: get_scienceDirect_info(article['link']), tqdm(scienceDirect_articles))
        )
    return scienceDirect_articles


def get_articles(user_id):
    """Get a list of all scholar articles published in IEEE, ACM, Springer and Science Direct"""
    # if user_id not in config.context_dict:
    #     config.context_dict[user_id] = {'stop': False, 'prog': 0,
    #                                     'count': 0, 'spin': False, 'saved_in_database': False}
    #
    # if not config.context_dict[user_id]['stop']:
    try:
        scholar_articles, all_articles_count = get_scholar_articles(user_id)
    except Exception as inst:
        print("could not get scholar articles", inst)
        return [], 0

    print('allllllll article count', all_articles_count)
    articles = []

    try:
        articles.extend(get_ieee_articles(scholar_articles, user_id))
        config.context_dict[user_id]['prog'] = config.context_dict[user_id]['prog'] + 5
    except:
        print("could not get ieee articles")

    try:
        articles.extend(get_springer_articles(scholar_articles, user_id))
        config.context_dict[user_id]['prog'] = config.context_dict[user_id]['prog'] + 5
    except:
        print("could not get springer articles")

    try:
        articles.extend(get_ACM_articles(scholar_articles, user_id))
        config.context_dict[user_id]['prog'] = config.context_dict[user_id]['prog'] + 5
    except:
        print("could not get ACM articles")

    try:
        articles.extend(get_scienceDirect_articles(scholar_articles, user_id))
        config.context_dict[user_id]['prog'] = config.context_dict[user_id]['prog'] + 5
    except:
        print("could not get science direct articles")

    return articles, all_articles_count

