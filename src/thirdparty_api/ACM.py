import requests
from bs4 import BeautifulSoup

from parsers.bibliographic import parse_bibliographic, remove_html_tags


def get_ACM_info(link):
    """Get an ACM article references and keywords"""
    try:
        response = requests.request("GET", link)
        if response.status_code != 200:
            raise RuntimeError('HTTP error : ' + response.reason)

        soup = BeautifulSoup(response.text, features="html5lib")

        # get keywords
        article_keywords = []
        tags = soup.select_one('.tags-widget__content')
        print(tags)
        if tags is not None:
            keywords = tags.contents[0]
            if keywords is not None:
                for item in keywords.contents:
                    keywords_text = item.contents[0].text
                    article_keywords.append(keywords_text)

        # get references
        refs = []
        reference = soup.select_one('.references__list')
        if reference is not None:
            for item in reference.contents:
                ref_text = item.select_one('.references__note')
                if ref_text is not None:
                    ref = remove_html_tags(ref_text.text)
                    refs.append(ref)
        parsed_ref = parse_bibliographic(refs)
        for i in range(0, len(parsed_ref)):
            if parsed_ref[i]:
                if 'title' in parsed_ref[i]:
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].strip('\"')
                    parsed_ref[i]['title'] = parsed_ref[i]['title'].lower()

        title = ""
        title_element = soup.select_one('.citation__title')
        if title_element is not None:
            title = title_element.text

        authors_tag = soup.select('.loa__author-name')
        authors = ''
        for author in authors_tag:
            if author.text not in authors:
                authors = authors + author.text + ', '

        publication = soup.select('.epub-section__title')
        if publication is not None:
            publication = publication[0].text

        info = soup.select('.issue-item__detail')
        if info is not None:
            info = info[0].text

        Date = soup.select('.epub-section__date')[0].text

        DOI = soup.select('.issue-item__doi')
        if DOI is not None:
            DOI = DOI[0].text

        title = authors + '"' + title + '"' + ', ' + publication + ', ' + Date + ', ' + DOI
        # article[ 'articleVolume'] + '(' + article['articleIssue'] + '), pp. ' + article['startPage'] + '-' + \
        # article['endPage']

        return {
            'title': title,
            'citation': parsed_ref,
            'keywords': article_keywords
        }

    except:
        pass


