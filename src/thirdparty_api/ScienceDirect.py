import requests
from bs4 import BeautifulSoup
import regex


def get_scienceDirect_info(link):
    """Get an Scince Direct article references and keywords"""
    identity = link.rstrip('/').split('/')[-1]
    headers = {
        'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
    }
    session = requests.session()
    response = session.request('GET', link, headers=headers)
    if response.status_code != 200:
        raise RuntimeError('HTTP error : ' + response.reason)
    soup = BeautifulSoup(response.text, features="html5lib")
    # title_meta = soup.select('meta[property="og:title"]')
    # article_title = ''
    # if title_meta is not None and len(title_meta) > 0 and 'content' in title_meta[0]:
    #     article_title = title_meta[0]['content']

    title_t = soup.select('.title-text')[0]
    article_title = title_t.text

    authors_tag = soup.select('.content')
    authors = ''
    for author in authors_tag:
        authors = authors + author.text[:-1] + ', '

    publication = soup.select('.publication-title')
    if publication is not None:
        publication = publication[0].text
    info = soup.select('.text-xs')
    if info is not None:
        info = info[0].text

    DOI = soup.select('.doi')
    if DOI is not None:
        DOI = DOI[0].text

    title = authors + '"' + article_title + '"' + ', ' + publication + ', ' + info + ', ' + DOI
            # article[ 'articleVolume'] + '(' + article['articleIssue'] + '), pp. ' + article['startPage'] + '-' + \
            # article['endPage']

    # get references
    ref = []
    reg = r'entitledToken":"([^"]+)'
    matches = regex.findall(reg, response.text)
    if len(matches) > 0:
        entitled_token = matches[0]
        ref_link = 'https://www.sciencedirect.com/sdfe/arp/pii/' + str(identity) + '/references?entitledToken=' + str(
            entitled_token)
        ref_response = session.request('GET', ref_link, headers=headers)
        if ref_response.status_code != 200:
            raise RuntimeError('HTTP error : ' + ref_response.reason)
        # soup = BeautifulSoup(ref_response.text, features="html5lib")
        ref_json = ref_response.json()
        if 'content' in ref_json and len(ref_json['content']) > 0 and '$$' in ref_json['content'][0] and len(ref_json['content'][0]['$$']) > 1 and '$$' in ref_json['content'][0]['$$'][1]:
            bib_reference = ref_json['content'][0]['$$'][1]['$$']
            for item in bib_reference:
                try:
                    if '$$' in item['$$'][1]['$$'][0] and '$$' in item['$$'][1]['$$'][0]['$$'][1]:
                        title = item['$$'][1]['$$'][0]['$$'][1]['$$'][0]['_']
                        ref.append({'title': title})
                except:
                    print("could not get reference")
                    pass

    # get keywords
    article_keywords = []
    keywords = soup.select_one('.keywords-section')
    # keywords = tags.contents[0]
    if keywords is not None:
        for item in keywords.contents:
            if item.name != 'h2' and item.contents is not None and len(item.contents) > 0:
                keywords_text = item.contents[0].text
                article_keywords.append(keywords_text)

    return {
        'title': title,
        'citation': ref,
        'keywords': article_keywords,
    }