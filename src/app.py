from database.model import Model
from flask import Flask, jsonify, request
import urllib.parse as urlparse
from urllib.parse import parse_qs
from flask_cors import cross_origin
from services.comapre import find_best_articles, find_best_articles_for_one
from services.send_email import send_email_directly
from thirdparty_api.solo_articles import get_one_article_info
from thirdparty_api.journal_api import get_journal_articles, get_one_journal_articles
from thirdparty_api.scholar_papers import get_articles
import config
from thirdparty_api.scholar import get_articles_number

SCHOLAR = []

model = Model(config.database_uri)
app = Flask(__name__)


@app.route('/scholar', methods=['GET', 'POST'])
@cross_origin()
def application():
    """Endpoint to check if scholar articles is saved in database and if it is, find similar articles, if it's
     not saved, to save scholar's articles and find similar artilcles"""
    response_object = {'status': 'success'}
    if request.method == 'POST':
        # try:
        post_data = request.json
        parsed = urlparse.urlparse(post_data.get('scholarLink'))
        qs = parse_qs(parsed.query)['user']
        if qs is None or len(qs) == 0:
            print('Give me Link')
            response_object['form'] = False
        else:
            # try:
                user_id = qs[0]
                config.context_dict[user_id] = {'stop': False, 'prog': 0,
                                                'count': 0, 'spin': False, 'saved_in_database': False}
                print(config.context_dict.keys())
                new_article_detected = False
                scholar_exists = model.check_scholar_exists(user_id)
                if scholar_exists:
                    latest_articles_count = model.scholar_articles_size(user_id)
                    scholar_articles_count_on_google = get_articles_number(user_id)
                    print(latest_articles_count, scholar_articles_count_on_google)
                    if latest_articles_count < scholar_articles_count_on_google:
                        new_article_detected = True
                parameters = post_data.get('parameters')
                print(scholar_exists)
                if scholar_exists and not new_article_detected:
                    config.context_dict[user_id]['spin'] = False
                    config.context_dict[user_id]['saved_in_database'] = True
                    model.update_scholar_contact(user_id, post_data.get('scholarName'), post_data.get('scholarEmail'))
                    res = find_best_articles(user_id, parameters)
                    model.update_scholar_email(user_id, res)
                    if not config.context_dict[user_id]['stop']:
                        print('\n mail is ready')
                        config.context_dict[user_id]['saved_in_database'] = False
                elif not scholar_exists or new_article_detected:
                    if not config.context_dict[user_id]['stop']:
                        model.update_scholar_contact(user_id, post_data.get('scholarName'), post_data.get('scholarEmail'))
                        print(user_id)
                        config.context_dict[user_id]['spin'] = True
                        # save_scholar_articles(user_id)
                        scholar, all_articles_count = get_articles(user_id)
                        model.update_scholar_article(user_id,scholar,all_articles_count)
                        config.context_dict[user_id]['spin'] = False
                        config.context_dict[user_id]['saved_in_database'] = True
                        print('\n saved to db')
                        res = find_best_articles(user_id, parameters)
                        if not config.context_dict[user_id]['stop']:
                            if res is not None:
                                model.update_scholar_email(user_id, res)
                                print('\n mail is ready')
                        config.context_dict[user_id]['saved_in_database'] = False

                        response_object['user'] = user_id
                        response_object['form'] = True
            # except:
            #     print("error in application")
            #     response_object['form'] = False

    else:
        response_object['scholar'] = SCHOLAR
    return jsonify(response_object)


@app.route('/recommended', methods=['GET', 'POST'])
@cross_origin()
def recommended():
    """Endpoint to send scholars recommendation to scholar's email on POST and to Show Email text in UI fon GET"""
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.json
        send_email_directly(post_data.get('data'), post_data.get('scholarEmail'))
        print('\n email has been sent')
        response_object['message'] = 'scholar added!'
        return jsonify(response_object)

    else:
        scholarLink = request.args.get('scholar')
        parsed = urlparse.urlparse(scholarLink)
        qs = parse_qs(parsed.query)
        if 'user' not in qs or len(qs['user']) == 0:
            result = ['کاربر موجود نیست']
            return jsonify(result)
        user_id = qs['user'][0]
        print(user_id)
        res = model.get_email_text(user_id)
        # print(res)
        recom = res['recommendations']
        print(recom)
        if len(recom) > 0:

            result = [x['Suggested papers published in AJEE'] for x in recom]
            # result = [x['Suggested papers published in AUT journal'] for x in RECOMMENDED]
            result = list(set(result))
            return jsonify(result)
        else:
            result = ['مقاله‌ی مشابهی یافت نشد']
            return jsonify(result)


@app.route('/progress', methods=['GET', 'POST'])
@cross_origin()
def progress():
    """Endpoint get a scholar state progress information and post it to UI"""
    scholarLink = request.args.get('scholar')
    parsed = urlparse.urlparse(scholarLink)
    qs = parse_qs(parsed.query)
    if 'user' not in qs or len(qs['user']) == 0:
        result = {'status': 'error'}
        return jsonify(result)
    user_id = qs['user'][0]

    if request.method == 'POST':
        post_data = request.json
        config.context_dict[user_id]['prog'] = post_data.get('prog')
        config.context_dict[user_id]['count'] = post_data.get('count')
        config.context_dict[user_id]['stop'] = post_data.get('stop')
        config.context_dict[user_id]['spin'] = post_data.get('spin')
        config.context_dict[user_id]['saved_in_database'] = post_data.get('saved')
        print(config.context_dict[user_id]['stop'])
        config.context_dict.pop(user_id)
        return jsonify({'status': 'success'})

    else:
        if user_id in config.context_dict:
            res = {'var': config.context_dict[user_id]['prog'],
                   'saved': config.context_dict[user_id]['saved_in_database'],
                   'spin': config.context_dict[user_id]['spin']}
            return jsonify(res)
        else:
            res = {'var': 0, 'saved': config.context_dict[user_id]['saved_in_database'],
                   'spin': config.context_dict[user_id]['spin']}
            return jsonify(res)


@app.route('/updatejournal', methods=['GET', 'POST'])
@cross_origin()
def updateJournal():
    """Endpoint to update Journal articles in database upon request"""
    if request.method == 'POST':
        post_data = request.json
        update_flag = post_data.get('update')
        if update_flag:
            get_journal_articles()
        return jsonify({'status': 'success'})


@app.route('/selectModal', methods=['GET', 'POST'])
@cross_origin()
def selectModal():
    """Endpoint to get a scholars recommendation from database and show it in UI"""
    scholarLink = request.args.get('scholar')
    parsed = urlparse.urlparse(scholarLink)
    qs = parse_qs(parsed.query)
    if 'user' not in qs or len(qs['user']) == 0:
        result = {'status': 'error'}
        return jsonify(result)
    user_id = qs['user'][0]

    if request.method == 'GET':
        print(user_id)
        res = model.get_email_text(user_id)
        # print(res)
        recom = res['recommendations']
        if len(recom) > 0:
            return {"recom": recom}
        else:
            result = ['مقاله‌ی مشابهی یافت نشد']
            return jsonify(result)

    if request.method == 'POST':
        post_data = request.json
        recoms = post_data.get('res')
        model.update_scholar_email(
            user_id,
            recoms)
        result = ['successfully updated']
        return jsonify(result)

@app.route('/comparesolo', methods=['POST'])
@cross_origin()
def oneCompare():
    """Endpoint to get an aticles link from UI and find similar articles to that specific article"""
    post_data = request.json
    articleTitle = post_data.get('articleTitle')
    articleKeywords = post_data.get('articleKeywords')
    articleAuthorEmail = post_data.get('articleAuthorEmail')
    articleReferences = post_data.get('articleReferences')
    parameters = post_data.get('param')
    config.context_dict[articleAuthorEmail] = {'stop': False, 'prog': 0,
                                    'count': 0, 'spin': False, 'saved_in_database': False}
    article_info = get_one_article_info(articleTitle, articleReferences, articleKeywords)
    print(article_info)
    recommended_articles = find_best_articles_for_one(article_info, parameters)
    if recommended_articles is not None:
        model.update_solo_email(articleAuthorEmail, recommended_articles)
    result = {'recom' : recommended_articles, 'stat': 'success'}
    print(result)
    return jsonify(result)

@app.route('/sendEmailsolo', methods=['POST'])
@cross_origin()
def sendSoloEmail():
    response_object = {'status': 'success'}
    post_data = request.json
    send_email_directly(post_data.get('data'), post_data.get('scholarEmail'))
    print('\n email has been sent')
    config.context_dict.pop(post_data.get('scholarEmail'))
    response_object['message'] = 'email sent!'
    return jsonify(response_object)

if __name__ == '__main__':
    if model.count_journal_entry() == 0:
        get_journal_articles()
    app.run(host='0.0.0.0')
